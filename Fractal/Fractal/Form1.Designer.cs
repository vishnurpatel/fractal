﻿namespace Fractal
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.colourChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fowardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setColourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pinkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orginalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cycleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cycleStopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFractelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fractelCycelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fractelStopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.animastionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCurrentStateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colourChangeToolStripMenuItem,
            this.changeFractelToolStripMenuItem,
            this.animastionToolStripMenuItem,
            this.saveToolStripMenuItem1,
            this.restToolStripMenuItem,
            this.testToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(624, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // colourChangeToolStripMenuItem
            // 
            this.colourChangeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fowardToolStripMenuItem,
            this.backToolStripMenuItem,
            this.setColourToolStripMenuItem,
            this.cycleToolStripMenuItem1,
            this.cycleStopToolStripMenuItem});
            this.colourChangeToolStripMenuItem.Name = "colourChangeToolStripMenuItem";
            this.colourChangeToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.colourChangeToolStripMenuItem.Text = "Colour Change";
            // 
            // fowardToolStripMenuItem
            // 
            this.fowardToolStripMenuItem.Name = "fowardToolStripMenuItem";
            this.fowardToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.fowardToolStripMenuItem.Text = "Next";
            this.fowardToolStripMenuItem.Click += new System.EventHandler(this.fowardToolStripMenuItem_Click);
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.backToolStripMenuItem.Text = "Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.backToolStripMenuItem_Click);
            // 
            // setColourToolStripMenuItem
            // 
            this.setColourToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blueToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.pinkToolStripMenuItem,
            this.yellowToolStripMenuItem,
            this.orginalToolStripMenuItem});
            this.setColourToolStripMenuItem.Name = "setColourToolStripMenuItem";
            this.setColourToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.setColourToolStripMenuItem.Text = "Set Colour";
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // pinkToolStripMenuItem
            // 
            this.pinkToolStripMenuItem.Name = "pinkToolStripMenuItem";
            this.pinkToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.pinkToolStripMenuItem.Text = "Pink";
            this.pinkToolStripMenuItem.Click += new System.EventHandler(this.pinkToolStripMenuItem_Click);
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.yellowToolStripMenuItem.Text = "Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler(this.yellowToolStripMenuItem_Click);
            // 
            // orginalToolStripMenuItem
            // 
            this.orginalToolStripMenuItem.Name = "orginalToolStripMenuItem";
            this.orginalToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.orginalToolStripMenuItem.Text = "Orginal";
            this.orginalToolStripMenuItem.Click += new System.EventHandler(this.orginalToolStripMenuItem_Click);
            // 
            // cycleToolStripMenuItem1
            // 
            this.cycleToolStripMenuItem1.Name = "cycleToolStripMenuItem1";
            this.cycleToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.cycleToolStripMenuItem1.Text = "Cycle Colour";
            this.cycleToolStripMenuItem1.Click += new System.EventHandler(this.cycleToolStripMenuItem1_Click);
            // 
            // cycleStopToolStripMenuItem
            // 
            this.cycleStopToolStripMenuItem.Name = "cycleStopToolStripMenuItem";
            this.cycleStopToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.cycleStopToolStripMenuItem.Text = "Cycle Stop";
            this.cycleStopToolStripMenuItem.Click += new System.EventHandler(this.cycleStopToolStripMenuItem_Click);
            // 
            // changeFractelToolStripMenuItem
            // 
            this.changeFractelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nextToolStripMenuItem,
            this.backToolStripMenuItem1,
            this.fractelCycelToolStripMenuItem,
            this.fractelStopToolStripMenuItem});
            this.changeFractelToolStripMenuItem.Name = "changeFractelToolStripMenuItem";
            this.changeFractelToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.changeFractelToolStripMenuItem.Text = "Change Fractel";
            // 
            // nextToolStripMenuItem
            // 
            this.nextToolStripMenuItem.Name = "nextToolStripMenuItem";
            this.nextToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.nextToolStripMenuItem.Text = "Next";
            this.nextToolStripMenuItem.Click += new System.EventHandler(this.nextToolStripMenuItem_Click);
            // 
            // backToolStripMenuItem1
            // 
            this.backToolStripMenuItem1.Name = "backToolStripMenuItem1";
            this.backToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
            this.backToolStripMenuItem1.Text = "Back";
            this.backToolStripMenuItem1.Click += new System.EventHandler(this.backToolStripMenuItem1_Click);
            // 
            // fractelCycelToolStripMenuItem
            // 
            this.fractelCycelToolStripMenuItem.Name = "fractelCycelToolStripMenuItem";
            this.fractelCycelToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.fractelCycelToolStripMenuItem.Text = "Fractel Cycel";
            this.fractelCycelToolStripMenuItem.Click += new System.EventHandler(this.fractelCycelToolStripMenuItem_Click);
            // 
            // fractelStopToolStripMenuItem
            // 
            this.fractelStopToolStripMenuItem.Name = "fractelStopToolStripMenuItem";
            this.fractelStopToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.fractelStopToolStripMenuItem.Text = "Fractel Stop";
            this.fractelStopToolStripMenuItem.Click += new System.EventHandler(this.fractelStopToolStripMenuItem_Click);
            // 
            // animastionToolStripMenuItem
            // 
            this.animastionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem1,
            this.stopToolStripMenuItem1});
            this.animastionToolStripMenuItem.Name = "animastionToolStripMenuItem";
            this.animastionToolStripMenuItem.ShowShortcutKeys = false;
            this.animastionToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.animastionToolStripMenuItem.Text = "Animastion";
            // 
            // startToolStripMenuItem1
            // 
            this.startToolStripMenuItem1.Name = "startToolStripMenuItem1";
            this.startToolStripMenuItem1.Size = new System.Drawing.Size(98, 22);
            this.startToolStripMenuItem1.Text = "Start";
            this.startToolStripMenuItem1.Click += new System.EventHandler(this.startToolStripMenuItem1_Click);
            // 
            // stopToolStripMenuItem1
            // 
            this.stopToolStripMenuItem1.Name = "stopToolStripMenuItem1";
            this.stopToolStripMenuItem1.Size = new System.Drawing.Size(98, 22);
            this.stopToolStripMenuItem1.Text = "Stop";
            this.stopToolStripMenuItem1.Click += new System.EventHandler(this.stopToolStripMenuItem1_Click);
            // 
            // saveToolStripMenuItem1
            // 
            this.saveToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToImageToolStripMenuItem,
            this.saveCurrentStateToolStripMenuItem});
            this.saveToolStripMenuItem1.Name = "saveToolStripMenuItem1";
            this.saveToolStripMenuItem1.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem1.Text = "Save";
            // 
            // saveToImageToolStripMenuItem
            // 
            this.saveToImageToolStripMenuItem.Name = "saveToImageToolStripMenuItem";
            this.saveToImageToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.saveToImageToolStripMenuItem.Text = "Save to Image";
            this.saveToImageToolStripMenuItem.Click += new System.EventHandler(this.saveToImageToolStripMenuItem_Click);
            // 
            // saveCurrentStateToolStripMenuItem
            // 
            this.saveCurrentStateToolStripMenuItem.Name = "saveCurrentStateToolStripMenuItem";
            this.saveCurrentStateToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.saveCurrentStateToolStripMenuItem.Text = "Save Current State";
            this.saveCurrentStateToolStripMenuItem.Click += new System.EventHandler(this.saveCurrentStateToolStripMenuItem_Click);
            // 
            // restToolStripMenuItem
            // 
            this.restToolStripMenuItem.Name = "restToolStripMenuItem";
            this.restToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.restToolStripMenuItem.Text = "Rest";
            this.restToolStripMenuItem.Click += new System.EventHandler(this.restToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.testToolStripMenuItem.Text = "Pallet";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.startToolStripMenuItem.Text = "Start";
            this.startToolStripMenuItem.Click += new System.EventHandler(this.startToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem colourChangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fowardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setColourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pinkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveCurrentStateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cycleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changeFractelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem animastionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fractelCycelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cycleStopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fractelStopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orginalToolStripMenuItem;
    }
}

