﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        // start copy from heare
        int c;
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;

        private Timer colour2 = new Timer(); // Timer used for duncasn way to change colour
        private Timer colour = new Timer();// timer for orgianl colour chance way
        private Timer ani = new Timer(); // timer used for fractal animaion
        private Timer anicol = new Timer(); // timer used for Full fractal animaion

        double test; // used to change colour of fractle (orgianl way)
        int t = 0; // Used to control my colour change
        int a = 0; // used to control animasion loop
        int ca = 0; // Contraol verible used for Full Colour And fractal Animaion
      
        Graphics g1; //declared as an instance variable so we can use it throuighout the class
        Graphics g2; // used to draw a new image ontop of the orginal 
        Bitmap myBitmap;
        Bitmap zoommap;

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
           //put the bitmap on the window
            Graphics windowG = e.Graphics;
            windowG.DrawImageUnscaled(myBitmap, 0, 0, x1, y1);
            windowG.DrawImageUnscaled(zoommap, 0, 0, x1, y1);
            update(e.Graphics);         
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                init();
                mandelbrot();
                rectangle = false;
                Refresh();
            }
        } // Used to take the last codrnets of the screen appon mouse button up
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                
                Refresh();
            }
        } 
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                rectangle = true; // move from mose move, as we want it to active on a mouse click not when the mouse is moved
                xs = e.X;
                ys = e.Y;
            }
        } // Used for when mouse button is clicked to start the rectangle
        private void Form1_Resize(object sender, EventArgs e)
        {
            init();
            mandelbrot();

        } // Used to rezise the bitmap on Rezie of the Screen.
        private void Form1_Load_1(object sender, EventArgs e)
        {
            System.IO.StreamReader file1 = new System.IO.StreamReader("c:\\Fractalsave.txt");

            // read lines of text
            string xss = file1.ReadLine();
            string xzs = file1.ReadLine();
            string yss = file1.ReadLine();
            string yzs = file1.ReadLine();
            string cs = file1.ReadLine();
            string ts = file1.ReadLine();

            //Convert the strings to int
            double.TryParse(xss, out xstart);
            double.TryParse(xzs, out xzoom);
            double.TryParse(yss, out ystart);
            double.TryParse(yzs, out yzoom);
            int.TryParse(cs, out c);
            double.TryParse(ts, out test);

            // close the stream
            file1.Close();
            mandelbrot();
            Refresh();
        } // used to load last state on LOAD of the program.

/// ////////////////////////

        private void fowardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = c + 10;
            mandelbrot();          
            Refresh();
        }
        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = c - 10;
            mandelbrot();;
            Refresh();
        }
        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = 150;
            mandelbrot();
            Refresh();
        }
        private void pinkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = 195;
            mandelbrot();
            Refresh();
        }
        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = 52;
            mandelbrot();
            Refresh();
        }
        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = 30;
            mandelbrot();
            Refresh();
        }
        private void orginalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            c = 0;
        }

        private void cycleToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            colour.Tick += new EventHandler(timer_colour);
            colour.Interval = 500;// in miliseconds
            colour.Start();

        } // starts the timer for colour chance cycle
        private void timer_colour(object sender, EventArgs e)
        {
            cycleToolStripMenuItem1.Text = "Faster Change";
            if (t == 0)
            {
                c = c + 4;
                if (c >= 230) { t = 1; };


            }
            if (t == 1)
            {
                c = c - 4;
                if (c <= 1) { t = 0; };

            }
            mandelbrot();
            Refresh();
        }// the funcion the timer calls to change the colour at set intervels
        private void cycleStopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour.Stop();
            cycleToolStripMenuItem1.Text = "Cycle Colour";
        } // stop the timer button

/// /////////////////////////////////////////////////////

        private void nextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            test = test + 0.03;
            mandelbrot();
        }
        private void backToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            test = test - 0.03;
            mandelbrot();
        }

        private void fractelCycelToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ani.Tick += new EventHandler(timer_ani);
            ani.Interval = 500;// in miliseconds
            ani.Start();

        } // Timer Start for Fractal Ainimaion
        private void timer_ani(object sender, EventArgs e)
        {
            fractelCycelToolStripMenuItem.Text = "Faster Change";
            if (a == 0)
            {
                test = test + 0.01;
                if (test >= 1) { a = 1; };


            }
            if (a == 1)
            {
                test = test - 0.01;
                if (test <= 0.01) { a = 0; };

            }
            mandelbrot();
            Refresh();
        } // menthord for fractal ainimaion.
        private void fractelStopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ani.Stop();
            fractelCycelToolStripMenuItem.Text = "Fractal Cycle";
        } // stop the timer button


/// //////////////////////
        private void startToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            anicol.Tick += new EventHandler(timer_anicol);
            anicol.Interval = 500;// in miliseconds
            anicol.Start();
        }
        public void timer_anicol(object sender, EventArgs e)
        {

            startToolStripMenuItem1.Text = "Faster";
            if (ca == 0)
            {
                test = test + 0.01;
                c = c + 2;
                if (test >= 1) { ca = 1; };


            }
            if (ca == 1)
            {
                test = test - 0.01;
                c = c - 2;
                if (test <= 0.01) { ca = 0; };

            }
            mandelbrot();
            Refresh();


        }
        private void stopToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            anicol.Stop();
            startToolStripMenuItem1.Text = "Start";
        } // Timer Stop for Full Animaion

/// ////////////////

        private void saveCurrentStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\Fractalsave.txt");
            file.WriteLine(xstart);
            file.WriteLine(xzoom);
            file.WriteLine(ystart);
            file.WriteLine(yzoom);
            file.WriteLine(c);
            file.WriteLine(test);

            file.Close();
        }
        private void saveToImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "fractal";
            sfd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            sfd.ShowDialog();

            System.IO.FileStream fileS = (System.IO.FileStream)sfd.OpenFile();
            this.myBitmap.Save(fileS, System.Drawing.Imaging.ImageFormat.Jpeg);

            fileS.Close();

            try
            {
                if (myBitmap != null)
                {
                    this.myBitmap.Save(fileS, System.Drawing.Imaging.ImageFormat.Jpeg);

                    //myBitmap.Save("c:\\Fractal.bmp");
                    saveToImageToolStripMenuItem.Text = "Saved file.";
                }
            }
            catch (Exception)
            {
                MessageBox.Show("There was a problem saving the file." +
                    "Check the file permissions.");
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (startToolStripMenuItem.Text == "Start")
            {
                colour2.Tick += new EventHandler(timer_colour2);
                colour2.Interval = 500;// in miliseconds
                colour2.Start();
                startToolStripMenuItem.Text = "Stop";
            }
            else if (startToolStripMenuItem.Text == "Stop")
            {
                colour2.Stop();
                startToolStripMenuItem.Text = "Start";
            }
           

        }
        private void timer_colour2(object sender, EventArgs e)
        {
            //memory steam object
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            // convert mybitmap to gif and save to the stream
            myBitmap.Save(stream, ImageFormat.Gif);
            // store gif image to bitmap object using data from stream
            myBitmap = new Bitmap(stream);
            // get palette information from img object (256)
            ColorPalette palette = myBitmap.Palette;
            // rotate palette
            for (int i = 0; i < palette.Entries.Length; i++)
            {
                if (i == 255)
                {
                    palette.Entries[i] = palette.Entries[0];
                }
                else
                {
                    palette.Entries[i] = palette.Entries[i + 1];
                }
            }
            myBitmap.Palette = palette;
            this.Refresh();
        }

/// ////////////////

        private void restToolStripMenuItem_Click(object sender, EventArgs e)
        {
            test = 0;
            c = 0;
            start();

        }

        public void init() // all instances will be prepared
        {
            finished = false;
            x1 = this.Width;
            y1 = this.Height;
            xy = (float)x1 / (float)y1;
            myBitmap = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(myBitmap); // get the graphics contact so that we can draw on the bitmap
            zoommap = new Bitmap(x1, y1);
            g2 = Graphics.FromImage(zoommap);
            finished = true;
        }
        public void destroy()
        {
            if (finished)
            {
                g1 = null;           
            }
        }
        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }
        public void stop()
        {
        }       
        public void update(Graphics g2)
        {
            Pen p1 = new Pen(Color.White, 1); //Pen for the Zoom Box (white as its easier to see)
            //g.drawImage(picture, 0, 0, this);
            if (rectangle)
            {
                //g.setColor(Color.white);
                if (xs < xe)
                {
                    if (ys < ye) g2.DrawRectangle(p1, xs, ys, (xe - xs), (ye - ys));
                    else g2.DrawRectangle(p1, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g2.DrawRectangle(p1, xe, ys, (xs - xe), (ye - ys));
                    else g2.DrawRectangle(p1, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }
        private void mandelbrot() // calculate all points
        {
            Color color;
            Pen p = new Pen(Color.Black, 1);
            int x, y;
            float h, b, alt = 0.0f;

            action = false;          
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; 
                        color = HSBColor.FromHSB(new HSBColor(h * 255 + c, 0.8f * 255, b * 255));
                        alt = h;
                        p = new Pen(color);
                    }
                              g1.DrawLine(p, x, y, x + 1, y);

                }           
            action = true;
        }
        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0 +test, i = 0.0+test, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

    }// close of form 1
}
